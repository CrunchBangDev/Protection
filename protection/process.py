import logging
import os
import signal
import sys
import time
from traceback import TracebackException
from types import TracebackType
from typing import Optional

import psutil  # type: ignore

log = logging.getLogger()

class Process(object):
  """Models a system process and provides pidfile functions
  
  >>> Process('test')
  Process(pid: 0, name: test, program: False)
  
  >>> Process('test', new=True)  # doctest:+ELLIPSIS
  Process(pid: ..., name: test, program: ...python...)
  """
  def __init__(self, name: str, pid: int = 0, new: bool = False):
    self.name = name
    self.pidfile = f"{name}.pid"
    self._pid = pid or os.getpid() if new else 0

  @property
  def pid(self) -> int:
    """The process identifier (PID) of this process
    
    >>> Process('test').pid
    0

    >>> Process('test', new=True).pid > 0
    True
    """
    if self._pidfile_exists and self._pid == 0:
      self._pid = self.try_read_pidfile()
    return self._pid

  @property
  def program(self) -> str:
    """The name of this process (defaults to first command argument)
    
    >>> Process('test').program
    'False'
    
    >>> "python" in Process('test', new=True).program
    True
    """
    return str(self.cmd[0])

  @property
  def created(self) -> float:
    """When this process was created
    
    >>> type(Process('test', new=True).created)
    <class 'float'>
    """
    return float(self._Process.create_time())

  @property
  def age(self) -> float:
    """The age of this process
    
    >>> type(Process('test', new=True).age)
    <class 'float'>
    """
    return time.time() - time.mktime(time.localtime(self.created))

  @property
  def _pidfile_exists(self) -> bool:
    """Whether the pidfile exists on disk"""
    return os.path.isfile(self.pidfile)

  @property
  def _Process(self) -> psutil.Process:
    """Model of this process from psutil
    
    >>> type(Process('test', new=True)._Process)
    <class 'psutil.Process'>
    """
    return psutil.Process(self.pid)

  @property
  def exists(self) -> bool:
    """Whether the PID is a running process
    
    >>> Process('test').exists
    False
    
    >>> Process('test', new=True).exists
    True
    """
    return bool(psutil.pid_exists(self.pid))

  @property
  def cmd(self) -> list:
    """Command that launched this process"""
    try:
      return list(self._Process.cmdline())
    except psutil.NoSuchProcess:
      return [False]

  def die(self) -> bool:
    """Exit cleanly"""
    self.remove_pidfile()
    if self.pid == os.getpid():
      sys.exit()
    try:
      os.kill(self.pid, signal.SIGTERM)
    except Exception as e:
      log.exception("I can't be killed", exc_info=e)
      return False
    return not self.exists

  def try_read_pidfile(self) -> int:
    """Try to read pidfile from disk"""
    if self._pidfile_exists:
      with open(self.pidfile, "r") as f:
        try:
          return int(f.readline())
        except ValueError as e:
          log.warning("Unable to read pidfile", exc_info=e)
    return self._pid

  def write_pidfile(self, force: bool = False) -> bool:
    """Write pidfile to disk"""
    if force or not self._pidfile_exists:
      try:
        with open(self.pidfile, "w") as f:
          f.write(str(self.pid))
      except Exception as e:
        log.exception("Failed to write pidfile", exc_info=e)
        return False
      return True
    return False

  def remove_pidfile(self, force: bool = False) -> bool:
    """Delete pidfile, effectively removing protection"""
    pidfile_contents = self.try_read_pidfile()
    if pidfile_contents != os.getpid() and not force:
      log.debug("Prevented removal of another instance's pidfile")
      return False
    pidfile = self.__dict__.get("pidfile", False)
    if self._pidfile_exists:
      log.debug(f"Removing pidfile for {pidfile_contents}")
      try:
        os.remove(pidfile)
      except Exception as e:
        log.exception("Failed to remove pidfile", exc_info=e)
        return False
    return True

  def update_pidfile(self) -> bool:
    """Useful if a process has forked or otherwise changed PIDs
    
    Updating the pidfile updates the cached PID
    >>> Process('test').update_pidfile()
    True
    
    This results in behavior similar to passing new=True to __init__
    >>> Process('test').try_read_pidfile() > 0
    True
    
    >>> Process('test').remove_pidfile()
    True
    """
    self._pid = os.getpid()
    return self.remove_pidfile(True) and self.write_pidfile()

  def __repr__(self) -> str:
    """
    >>> Process('test')  # doctest:+ELLIPSIS
    Process(pid: ..., name: test, program: ...)
    """
    return f"Process(pid: {self.pid}, name: {self.name}, program: {self.program})"

  def __exit__(self, exc_type: Optional[BaseException], exc_value: Optional[TracebackException], traceback: Optional[TracebackType]) -> None:
    self.remove_pidfile()

if __name__ == '__main__':
  import doctest
  doctest.testmod()
