import logging
from . import Process

log = logging.getLogger()

class OverlapProtection(Process):
  """Provides pidfile-based process protection
  
  >>> OverlapProtection('test', 10)  # doctest:+ELLIPSIS
  Process(pid: ..., name: test, program: ...)
  
  >>> OverlapProtection('test', 10).pid > 0
  True
  
  >>> OverlapProtection('test', 10).exists
  True
  """
  def __init__(self, name: str, max_age: int):
    """Check pidfile to make sure another copy of us isn't running
    
    If there is another copy, check if it has aged out and if so kill it
    
    If it hasn't, kill self instead
    
    Otherwise, write pidfile and go back to whatever we were doing"""
    super().__init__(name, new=True)
    process = Process(name=name)
    if process._pidfile_exists:
      if process.exists and process.pid != self.pid:
        if self.name == process.name:
          if process.age > max_age:
            if not process.die():
              log.error(f"Failed to kill process: {process}")
              self.die()
            else:
              log.info(f"Killed stale process: {process}")
          else:  # Other instance hasn't aged out, kill self instead
            log.info(f"{name} started while already running! Exiting...")
            self.die()
        else:
          log.debug(f"Previous PID is in use but not by us: {process}")
      else:
        log.info("Detected stale PID file. Attempting to overwrite...")
    process.remove_pidfile(True)
    self.write_pidfile(force=True)

  def cleanup(self) -> None:
    """Remove pidfile before exit
    
    >>> OverlapProtection('test', 10).cleanup()
    
    """
    self.remove_pidfile()

if __name__ == '__main__':
  import doctest
  doctest.testmod()
