from typing import Type

from .process import Process
from .protection import OverlapProtection

def protect(name: str, max_age: int = 9999) -> OverlapProtection:
  return OverlapProtection(name, max_age)

__all__ = ['protect']