# Protection

A pidfile-based process protection tool

## Setup

```sh
pip install git+https://gitlab.com/CrunchBangDev/protection.git
```

## Usage

Call the `protect` function with a name and optionally a maximum process age:

```python
from protection import protect
protection = protect("me")

...

protection.cleanup()
```

Protection will place a file in the current directory containing your process'
identifier ("PID"). Rather, it will first check if such a file already exists.
If it does, the PID in the file is checked to see whether it still belongs to
an instance of the same app (using the name you provide); if so, that process'
age is checked to see whether it should be allowed to run, or it should be
stopped so that the new instance can run.

The effect is that by using the code above, you will not be able to start
multiple instances of the same script at the same time - after the first one,
they will exit or attempt to take over.

This is useful in cases where your code is run on a schedule (e.g. by crontab)
but isn't threadsafe, because who writes threadsafe Python?